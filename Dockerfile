ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/alplain
ARG FROM_IMG_NAME=openjdk-jre-headless
ARG FROM_IMG_TAG="2020-04-13-3"

FROM ${FROM_IMG_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}
VOLUME ["/opt/go-server/artifacts/serverBackups/"]
ENV GOCD_AGENT_AUTOENABLE_KEY=qnibFTW \
    GOCD_SERVER_CLEAN_WORKSPACE=false
ARG GOCD_URL=https://download.gocd.io/binaries
ARG GOCD_VER=22.2.0
ARG GOCD_SUBVER=14697
ARG CT_VER=0.29.2
ENV GLIBC_REPO=https://github.com/sgerrand/alpine-pkg-glibc
ENV GLIBC_VERSION=2.30-r0

LABEL gocd.version=${GOCD_VER}-${GOCD_SUBVER}
RUN apk --no-cache add curl git openssl xmlstarlet libarchive-tools libstdc++ curl ca-certificates moreutils openssh-client-default procps py3-pip \
 && for pkg in glibc-${GLIBC_VERSION} glibc-bin-${GLIBC_VERSION};do \
    curl -sSL ${GLIBC_REPO}/releases/download/${GLIBC_VERSION}/${pkg}.apk -o /tmp/${pkg}.apk; done \
 && apk add --allow-untrusted /tmp/*.apk \
 && rm -v /tmp/*.apk \
 && /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib \
 && pip install wildq

RUN export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo service-scripts --regex ".*\.tar" |head -n1) \
 && echo "# service-scripts: ${SRC_URL}" \
 && curl -Ls ${SRC_URL} |tar xf - -C /opt/
RUN mkdir -p /opt/go-server \
 && echo "go-server: https://download.go.cd/binaries/${GOCD_VER}-${GOCD_SUBVER}/generic/go-server-${GOCD_VER}-${GOCD_SUBVER}.zip" \
 && curl -Ls --url ${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-server-${GOCD_VER}-${GOCD_SUBVER}.zip |bsdtar xfz - -C /opt/go-server --strip-components=1
WORKDIR /opt/go-server/plugins/external/
# The layers are independently pushed, if they are combined one change will alter the content of the combined layer
RUN export GORG=gocd-contrib \
 && export GREPO=script-executor-task \
 && export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg ${GORG} --ghrepo ${GREPO} --regex '.*\.jar' |head -n1) \
 && wget -q ${SRC_URL}
RUN export GORG=manojlds \
 && export GREPO=gocd-docker \
 && export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1) \
 && wget -q ${SRC_URL}
RUN export GORG=ashwanthkumar \
 && export GREPO=gocd-build-github-pull-requests \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*github-pr-poller.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*git-fb-poller.*\.jar" |head -n1)
RUN export GORG=ind9 \
 && export GREPO=gocd-s3-artifacts \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3material-assembly.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3fetch-assembly.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3publish-assembly.*\.jar" |head -n1)
RUN export GORG=jmnarloch \
 && export GREPO=gocd-health-check-plugin \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
RUN export GORG=gocd-contrib \
 && export GREPO=docker-swarm-elastic-agents \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
RUN export GORG=tomzo \
 && export GREPO=gocd-yaml-config-plugin \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
RUN apk --no-cache add unzip \
 && curl -Lso /tmp/consul-template.zip https://releases.hashicorp.com/consul-template/${CT_VER}/consul-template_${CT_VER}_linux_amd64.zip \
 && cd /usr/local/bin \
 && unzip /tmp/consul-template.zip \
 && apk --no-cache del unzip \
 && rm -f /tmp/consul-template.zip
WORKDIR /root/
ADD opt/qnib/entry/10-gocd-restore.sh \
    opt/qnib/entry/11-gocd-config-init.sh \
    opt/qnib/entry/20-gocd-render-cruise-config.sh \
    /opt/qnib/entry/
ADD opt/qnib/gocd/server/etc/cruise-config.xml /opt/qnib/gocd/server/etc/
ENV ENTRYPOINTS_DIR=/opt/qnib/entry/ \
    GOCD_BACKUP_RESTORE_ENABLED=false
RUN chmod +x /opt/go-server/bin/go-server
CMD ["/opt/go-server/bin/go-server", "console"]
