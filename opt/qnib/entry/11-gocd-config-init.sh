#!/bin/bash
set -e

SERVER_INSTALLATION_DIR=/opt/go-server
CFG_DIR=/opt/go-server/config
SERVER_BKP_DIR=/opt/go-server/artifacts/serverBackups/

if [[ -f "${CFG_DIR}/cruise-config.xml"  ]];then
    echo ">> Config already present; drop out!"
    exit 0
fi

echo "### Start server to generate cruise-config.xml"
/opt/go-server/bin/go-server start

while [[ $(wildq -M -i xml -o yaml '.cruise.server."@tokenGenerationKey"' "${CFG_DIR}/cruise-config.xml") == "None" ]];do
    echo -n "."
    sleep 1
done
echo " OK"
 
echo "### Stop server"
/opt/go-server/bin/go-server stop

echo "### Change config"
if [[ -f "/etc/go/password.properties" ]];then
    cd /opt/go-server/config/
    wildq -M -i xml -o xml '.cruise.server.security.authConfigs.authConfig."@id" = "passwd"' cruise-config.xml | sponge cruise-config.xml
    wildq -M -i xml -o xml '.cruise.server.security.authConfigs.authConfig."@pluginId" = "cd.go.authentication.passwordfile"' cruise-config.xml | sponge cruise-config.xml
    wildq -M -i xml -o xml '.cruise.server.security.authConfigs.authConfig.property.key = "PasswordFilePath"' cruise-config.xml | sponge cruise-config.xml
    wildq -M -i xml -o xml '.cruise.server.security.authConfigs.authConfig.property.value = "/etc/go/password.properties"' cruise-config.xml | sponge cruise-config.xml
fi

### YAML Config
#PL_COUNT=0
#cd /opt/go-server/config/
#for pl in $(echo ${GOCD_YAML_REPOS} |tr "|" " ");do
#    PL_COUNT=$((${PL_COUNT}+1))
#    echo ">> PIPELINE: ${pl}"
#    NAME=$(echo ${pl} |awk -F'#' '{print $1}')
#    URL=$(echo ${pl} |awk -F'#' '{print $2}')
#    PATTERN=$(echo ${pl} |awk -F'#' '{print $3}')
#    if [[ ${PL_COUNT} -eq 1 ]];then
#        wildq -M -i xml -o xml '.cruise."config-repos"."config-repo" += {"@id": "CFG_NAME", "@pluginId": "yaml.config.plugin", "rules": {}}' cruise-config.xml |sed -e "s/CFG_NAME/${NAME}/" |sponge cruise-config.xml
#        wildq -M -i xml -o xml '.cruise."config-repos"."config-repo".rules += {"allow": "*"}' cruise-config.xml |sed -e 's/<allow>/<allow action="refer" type="*">/' |sponge cruise-config.xml
#        wildq -M -i xml -o xml '.cruise."config-repos"."config-repo".configuration.property.key = "file_pattern"' cruise-config.xml |sponge cruise-config.xml
#        wildq -M -i xml -o xml '.cruise."config-repos"."config-repo".configuration.property.value = "*.yml,*.yaml"' cruise-config.xml |sponge cruise-config.xml
#        wildq -M -i xml -o xml '.cruise."config-repos"."config-repo".git = "URL"' cruise-config.xml |sed -e "s#>URL.*# url=\"${URL}\" />#" |sponge cruise-config.xml
#    else 
#        echo '## more than one not implemented (yet)'
#    fi
#done
